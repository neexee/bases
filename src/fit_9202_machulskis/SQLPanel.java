package fit_9202_machulskis;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * User: violetta
 * Date: 5/14/12
 * Time: 6:01 PM
 */
public class SQLPanel extends JPanel
{
    JTextArea status;
    JTextField code;
    JButton sendButton;
    JButton clearButton;
    Database base;
    SQLPanel(Database base)
    {
        super();
        initPanel();
        this.base = base;
    }
    SQLPanel()
    {
        super();
        initPanel();
        //this.base = base;
    }
    void setBase(Database base)
    {
        this.base = base;
    }
    void initPanel()
    {
        setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        Border codeBorder = BorderFactory.createTitledBorder("SQL Code"); 
        Border statusBorder = BorderFactory.createTitledBorder("Status");

        status = new JTextArea("Status");
        code = new JTextField("select * from ");

        status.setBorder(statusBorder);
        code.setBorder(codeBorder);
        code.setEditable(true);
        status.setEditable(false);

        sendButton = new JButton("Send");
        sendButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                String query = code.getText();
                try
                {
                    Table rs =  base.sendSQL(query);

                    status.setText(rs.toString());
                  //  rs.toString();
                }
                catch(SQLException e)
                {
                    status.setText(e.getLocalizedMessage());
                }
                System.out.println("SEND");
            }
        });
        clearButton = new JButton("Clear");
        clearButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                code.setText("");
                status.setText("");
                System.out.println("CLEAR");
            }
        });
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1;
        c.weighty = 1;
        add(code, c);

        JPanel statusButtons = new JPanel();
        statusButtons.setLayout(new GridBagLayout());
        GridBagConstraints cc =new GridBagConstraints();
        cc.fill = GridBagConstraints.HORIZONTAL;
        cc.gridx =0;
        cc.gridy =0;

        statusButtons.add(status, cc);
        JPanel buttons = new JPanel();
        buttons.setLayout(new GridBagLayout());
        cc.gridy = 0;
        cc.gridx = 0;

        buttons.add(clearButton, cc);
        cc.gridx = 1;
        buttons.add(sendButton, cc);
        c.fill = GridBagConstraints.HORIZONTAL;
        c.anchor = GridBagConstraints.SOUTH;
        c.gridy = 1;
        c.gridx = 0;
        c.weighty = 0;
        statusButtons.add(buttons, c);

        add(statusButtons, c);

    }
    public void setResultString(String result)
    {
        status.setText(result);
    }


}
