package fit_9202_machulskis;

import java.sql.*;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Vector;

/**
 * User: violetta
 * Date: 5/13/12
 * Time: 7:39 PM
 */
public class Database
{
    private Connection connection;
    private DatabaseMetaData metaData;
    HashMap<String, Integer> mapTables = new HashMap<String,Integer>();
    Vector<Table> tables = new Vector<Table>();

    public void connect(String uri, String user, String password) throws Exception
    {
        Locale.setDefault(new Locale("en", "US"));
        DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());
        connection = DriverManager.getConnection(uri, user, password);
        System.out.println("CONNECTED");

        if(connection==null)
        {
            throw new Exception("Connection error");
        }
        metaData = connection.getMetaData();
    }

    public Vector<String> getTables() throws SQLException
    {
        Vector<String> vec = new Vector<String>();

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select TABLE_NAME from TABS");
        while(rs.next())
            vec.add(rs.getString("TABLE_NAME"));
        rs.close();
        rs = null;
        stmt.close();
        stmt = null;
        return vec;
    }

    public Vector<String> getColumns(String tableName) throws SQLException
    {
        Vector<String> vec = new Vector<String>();

        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select column_name, data_type from user_tab_columns where table_name = '" + tableName + "'");
        while(rs.next())
        {
            vec.add(rs.getString("column_name"));
        }
        rs.close();
        rs = null;
        stmt.close();
        stmt = null;
        return vec;
    }

    public Table getTable(String tableName) throws SQLException
    {
        if(mapTables.containsKey(tableName) && !tables.get(mapTables.get(tableName)).isDirty())
        {
            return tables.get(mapTables.get(tableName));
        }
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery("select * from " + tableName);

        ResultSetMetaData rsmd = rs.getMetaData();
        int count = rsmd.getColumnCount();
        Vector<String> columns = new Vector<String>(count);
        for(int i = 1; i<= count ; i++)
        {
            String columnName = rsmd.getColumnName(i);
            columns.add(columnName);
        }
        Table table = new Table(tableName, columns);

        while(rs.next())
        {
            Vector<String> row = new Vector<String>(count);
            for(int i = 1; i<= count ; i++)
            {
                String columnName = rsmd.getColumnName(i);
                row.add(rs.getString(columnName));

            }
            table.addRow(row);
        }
        ResultSet importedKeys = metaData.getImportedKeys(null, null, tableName);
        Table tmp = new Table(importedKeys);
        Vector<String> column= tmp.getColumn("FKCOLUMN_NAME");
        Vector<String> importTables = tmp.getColumn("PKTABLE_NAME");
        for(int i=0; i< column.size(); i++ )
        {
            getTable(importTables.get(i));
            table.addFKKey(column.get(i), importTables.get(i));
        }
        rs.close();
        rs = null;
        stmt.close();
        stmt = null;
        tables.add(table);
        int index = tables.indexOf(table);
        table.setDirtyFlag(false);
        mapTables.put(tableName, index);
        return table;
    }

    public void addEntry(String tableName, String entry) throws SQLException
    {
        Statement stmt = connection.createStatement();
        Vector<String> columns = getTable(tableName).columnNames;
        String first = "";
        for(int i =1; i< columns.size(); i++)
        {
            if(i==columns.size() -1)
            {
                first+=" "+columns.get(i);
            }
            else
            {
                first+=" " +columns.get(i)+",";
            }

        }
        String str = "INSERT INTO "+ tableName +  " ("+ first+ ") " +" VALUES(" + entry + ")";
        stmt.executeUpdate(str);
        Table t = tables.get(mapTables.get(tableName));
        t.setDirtyFlag(true);
        System.out.println(str);
    }

    public void updateEntry(String tableName, String columnName, String value, String columnIdName, String idValue) throws SQLException
    {
        Statement stmt = connection.createStatement();
        String str = "UPDATE " + tableName + " SET " + columnName + "='" + value + "' WHERE " + columnIdName + "='" + idValue+"'";
        stmt.executeUpdate(str);
        Table t = tables.get(mapTables.get(tableName));
        t.setDirtyFlag(true);
        System.out.println(str);
    }

    public void deleteEntry(String tableName, String columnIdName, String idValue) throws SQLException
    {
        Statement stmt = connection.createStatement();
        String str = "DELETE FROM " + tableName + " WHERE " + columnIdName + "='" + idValue+"'";
        stmt.executeUpdate(str);
        Table t = tables.get(mapTables.get(tableName));
        t.setDirtyFlag(true);
        System.out.println(str);
    }

    public Table sendSQL(String query) throws SQLException
    {
        Statement stmt = connection.createStatement();
        ResultSet rs = stmt.executeQuery(query);
        Table table =  new Table(rs);
        rs.close();
        return table;

    }

    public void disconnect() throws SQLException
    {
        connection.close();
    }

    public Connection getConnection()
    {
        return connection;
    }
    public void finalize()
    {
        try
        {
            connection.close();
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
    }
    public DatabaseMetaData getMetaData() throws SQLException
    {
        return connection.getMetaData();
    }
    public Vector<Vector<String>> mapTableWithIDsToTableWithDescriptions(Table tableb)
    {
        Vector<Vector<String>> data  = new Vector<Vector<String>>(tableb.getData());
        Vector<String> columns = tableb.columnNames;
        int columnIndex = 0;
        for(String c : columns)
        {
            String tname= tableb.getFKTable(c);
            if(tname != null)
            {
                Table t = null;
                try
                {
                    t = getTable(tname);
                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
                Vector<String> ids = tableb.getColumn(c);
                for(int i = 0; i< ids.size(); i++)
                {
                    String id = ids.get(i);
                    String mapped = t.mapIDToDescription(id);
                    if(mapped.equals(""))
                    {
                        continue;
                    }
                   // System.out.println("Mapped " + "'"+mapped+"'");
                    Vector<String> row = data.get(i);
                    row.set(columnIndex, new String(mapped));
                    data.set(i, row);
                }

            }
            columnIndex++;
        }
        return data;
    }
}

