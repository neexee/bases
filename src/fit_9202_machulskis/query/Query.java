package fit_9202_machulskis.query;

import fit_9202_machulskis.QueryPanel;

import javax.swing.*;
import java.util.Vector;
/**
 * User: violetta
 * Date: 5/27/12
 * Time: 5:16 AM
 */
public abstract class Query
{
    Vector<JCheckBox> checkBoxes = new Vector<JCheckBox>();
    Vector<JTextArea> textAreas = new Vector<JTextArea>();
    Vector<JComboBox> comboBoxes = new Vector<JComboBox>();
    String description = "";
    QueryPanel panel;
    Query(QueryPanel p)
    {
        panel = p;
    }
    void run()
    {
    }
    public abstract String generateQuery();
    public abstract String name();
    public abstract void init();
    public Vector<JCheckBox> getCheckBoxes()

    {
        return checkBoxes;
    }
    public Vector<JTextArea> getTextAreas()
    {
        return textAreas;
    }
    public String description()
    {
        return description;
    }
    public Vector<JComboBox> getComboBoxes()
    {
        return comboBoxes;
    }

}
