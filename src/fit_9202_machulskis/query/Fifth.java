package fit_9202_machulskis.query;

import fit_9202_machulskis.Database;
import fit_9202_machulskis.QueryPanel;
import fit_9202_machulskis.Table;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;

/**
 * User: violetta
 * Date: 5/28/12
 * Time: 7:07 PM
 */
public class Fifth  extends Query
{
    JComboBox areaBox;// = new JComboBox();

    public Fifth(QueryPanel p)
    {
        super(p);

    }

    @Override
    public String generateQuery()
    {
        String selectedArea = (String) areaBox.getSelectedItem();
        String query = "select technique_.id, technique_type.description from technique_ inner join technique_type on technique_.type = technique_type.id" +
                " inner join object on technique_.object = object.id" +
                " inner join area on area.id = object.area ";

        if(selectedArea.equals("ALL"))
        {
            return query;
        }
        if(!selectedArea.equals("ALL"))
        {
            query+=" where area.description ='" +selectedArea+"'";
        }
        return query;
    }

    @Override
    public String name()
    {
        return "Fifth";
    }

    @Override
    public void init()
    {

        Database base = panel.getBase();
        Table areaTable = null;
        try
        {
            areaTable = base.getTable("AREA");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        Vector<String> areas = new Vector<String>();
        int indexDescription = areaTable.getColumnNum("DESCRIPTION");
        Vector<Vector<String>> t = base.mapTableWithIDsToTableWithDescriptions(areaTable);
        for(Vector<String> row : t)
        {
            areas.add(row.get(indexDescription));
        }
        areaBox = new JComboBox(areas);
        areaBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                panel.refresh();
            }
        });

        areaBox.addItem("ALL");
        comboBoxes.add(areaBox);

    }
}
