package fit_9202_machulskis.query;

import fit_9202_machulskis.QueryPanel;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * User: violetta
 * Date: 5/27/12
 * Time: 5:21 AM
 */
public class First extends Query
{
    final String name = "First";
    JCheckBox area;
    public First(QueryPanel p)
    {
        super(p);
        area = new JCheckBox("Area");
        area.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                panel.refresh();
            }
        });
        //JCheckBox goverment = new JCheckBox("Goverment");
        checkBoxes.add(area);
    }
    @Override
    void run()
    {
        if(area.isSelected())
        {

        }
    }

    @Override
    public String generateQuery()
    {
        String query = "";
        if(area.isSelected())
        {
            query = "select area.goverment, area.id, area.description, peoples.lastname, peoples.firstname from area inner join peoples on peoples.id = area.head";
        }
        else
        {
            query ="select area.goverment from area";
        }
        return query;
    }

    @Override
    public String name()
    {
        return name;
    }

    @Override
    public void init()
    {
    }

}
