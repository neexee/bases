package fit_9202_machulskis.query;

import fit_9202_machulskis.Database;
import fit_9202_machulskis.QueryPanel;
import fit_9202_machulskis.Table;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Vector;

/**
 * User: violetta
 * Date: 5/28/12
 * Time: 12:37 AM
 */
public class Second extends Query
{
    JComboBox areaBox;// = new JComboBox();
    JComboBox govermentBox;

    public Second(QueryPanel p)
    {
        super(p);

    }

    @Override
    public String generateQuery()
    {
        String selectedArea = (String) areaBox.getSelectedItem();
        String selectedGov = (String)govermentBox.getSelectedItem();
        String query = "select ingeneers.id, ingeneer_types.description as type, area.description as area_, area.goverment, peoples.lastname, peoples.firstname" +
                " from ingeneers inner join area on area.id = ingeneers.area" +
                " inner join ingeneer_types on ingeneer_types.id = ingeneers.type"+
                " inner join peoples on peoples.id = ingeneers.people";

        if(selectedArea.equals("ALL") && selectedGov.equals("ALL"))
        {
          return query;
        }
        if(!selectedArea.equals("ALL"))
        {
            query+=" where area.description ='" +selectedArea+"'";
        }
        if(!selectedGov.equals("ALL") && !selectedArea.equals("ALL"))
        {
            query+=" and area.goverment = "+ selectedGov;
        }
        else if(!selectedGov.equals("ALL"))
        {
            query+= " where area.goverment = "+ selectedGov;
        }
        return query;
    }

    @Override
    public String name()
    {
        return "Second";
    }

    @Override
    public void init()
    {

        Database base = panel.getBase();
        Table areaTable = null;
        try
        {
            areaTable = base.getTable("AREA");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        Vector<String> areas = new Vector<String>();
        HashSet<String> govs = new HashSet<String>();
        int indexDescription = areaTable.getColumnNum("DESCRIPTION");
        int indexGov = areaTable.getColumnNum("GOVERMENT");
        Vector<Vector<String>> t = base.mapTableWithIDsToTableWithDescriptions(areaTable);
        for(Vector<String> row : t)
        {
            areas.add(row.get(indexDescription));
            govs.add(row.get(indexGov));
        }
        areaBox = new JComboBox(areas);
        areaBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                panel.refresh();
            }
        });
        govermentBox = new JComboBox(govs.toArray());
        govermentBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                panel.refresh();
            }
        });
        areaBox.addItem("ALL");

        govermentBox.addItem("ALL");
        comboBoxes.add(areaBox);

        comboBoxes.add(govermentBox);
    }
}
