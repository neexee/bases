package fit_9202_machulskis.query;

import fit_9202_machulskis.Database;
import fit_9202_machulskis.QueryPanel;
import fit_9202_machulskis.Table;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Vector;

/**
 * User: violetta
 * Date: 5/28/12
 * Time: 5:14 AM
 */
public class Third extends Query
{
    JComboBox areaBox;// = new JComboBox();

    public Third(QueryPanel p)
    {
        super(p);

    }

    @Override
    public String generateQuery()
    {
        String selectedArea = (String) areaBox.getSelectedItem();
        String query = "select object.id, area.description as area_, area.goverment" +
                " from  object inner join area on area.id = object.area";

        if(selectedArea.equals("ALL"))
        {
            return query;
        }
        if(!selectedArea.equals("ALL"))
        {
            query+=" where area.description ='" +selectedArea+"'";
        }
        return query;
    }

    @Override
    public String name()
    {
        return "Third";
    }

    @Override
    public void init()
    {

        Database base = panel.getBase();
        Table areaTable = null;
        try
        {
            areaTable = base.getTable("AREA");
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        Vector<String> areas = new Vector<String>();
        int indexDescription = areaTable.getColumnNum("DESCRIPTION");
        Vector<Vector<String>> t = base.mapTableWithIDsToTableWithDescriptions(areaTable);
        for(Vector<String> row : t)
        {
            areas.add(row.get(indexDescription));
        }
        areaBox = new JComboBox(areas);
        areaBox.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                panel.refresh();
            }
        });

        areaBox.addItem("ALL");
        comboBoxes.add(areaBox);

    }
}
