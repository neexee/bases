package fit_9202_machulskis;

import com.sun.org.apache.bcel.internal.generic.Select;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.util.Vector;

/**
 * User: violetta
 * Date: 5/15/12
 * Time: 2:51 PM
 */
public class TablesPanel extends JPanel
{
    JList tableList;
    Database base;
    JPanel tablePanel;
    private Vector<String> tables;
    int selectedTable = 0;
    JButton addNewEntryButton;
    JButton deleteEntry;
    Table tableb;
    int selectedEntryInList;
    Vector<Pair<String, Table>> mapTables;
    DefaultTableModel tm;
    JPanel buttonPanel;
    JTable table;

    public class MyComboBoxRenderer extends JComboBox implements TableCellRenderer
    {

        public MyComboBoxRenderer(Vector<String> items)
        {
            super(items);

        }
    /*
        protected void selectedItemChanged()
        {
            super.selectedItemChanged();
            System.out.println( "Selected, desu^"+getSelectedItem());
            selectedEntryInList = getSelectedIndex();
            System.out.println("Selected " + selectedEntryInList);
        }
      */
        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column)
        {
            if(isSelected)
            {
                setForeground(table.getSelectionForeground());
                super.setBackground(table.getSelectionBackground());
            }
            else
            {
                setForeground(table.getForeground());
                setBackground(table.getBackground());
            }

            setSelectedItem(value);
            return this;
        }


    }

    public class MyComboBoxEditor extends DefaultCellEditor
    {
        public MyComboBoxEditor(Vector<String> items)
        {
            super(new JComboBox(items));
        }
    }

    TablesPanel()
    {
        super();
        init();
    }

    void setBase(Database base)
    {
        this.base = base;
    }

    void connect()
    {

        try
        {
            initTableList();
           
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    void initButtonPanel()
    {
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new GridBagLayout());
        addNewEntryButton = new JButton("Add Entry");
        deleteEntry = new JButton("Delete entry");
        addNewEntryButton.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                int count = tm.getColumnCount();
                Vector<String> row = new Vector<String>();
                String entry = "";
                for(int i = 1; i< count; i++)
                {
                    row.add("New");
                    if(i==count-1)
                    {
                        entry += " '01'";
                    }
                    else
                    {
                        entry+=" '01',";
                    }
                }

                try
                {
                    base.addEntry(tableb.getName(),entry );
                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
                TablesPanel.this.repaintTablePanel();
            }
        });
        deleteEntry.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
               int row = table.getSelectedRow();
               TableColumn col = table.getColumn("ID");
               int idCol = col.getModelIndex();
                        
               String str = (String) tm.getValueAt(row, idCol);
                try
                {
                    base.deleteEntry(tableb.getName(), "ID", str);
                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
                TablesPanel.this.repaintTablePanel();
            }
        });
        buttonPanel.add(addNewEntryButton);
        buttonPanel.add(deleteEntry);
    }

    void init()
    {
        tableList = new JList();
        tablePanel = new JPanel();
        initButtonPanel();
        add(buttonPanel);
        add(tableList);
        add(tablePanel);
    }

    void initTableList() throws Exception
    {
        tables = base.getTables();
        tableList.setListData(tables);
        tableList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableList.setLayoutOrientation(JList.VERTICAL);
        tableList.setSelectedIndex(selectedTable);
        tableList.addListSelectionListener(new ListSelectionListener()
        {
            //@SuppressWarnings("unchecked")
            @Override
            public void valueChanged(ListSelectionEvent e)
            {
                tableList = (JList<String>) e.getSource();
                selectedTable = tableList.getSelectedIndex();
                repaintTablePanel();
            }
        });
    }

    void repaintTablePanel()
    {
        tablePanel.removeAll();
        Vector<String> columns = new Vector<String>();
        final String tableName = tables.get(selectedTable);
        tableb = null;
        try
        {
            tableb = base.getTable(tableName);
        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }
        columns = tableb.columnNames;
        Vector<Vector<String>> r = base.mapTableWithIDsToTableWithDescriptions(tableb);
        tm = new DefaultTableModel(r, columns)
        {
            private static final long serialVersionUID = 1L;

            public boolean isCellEditable(int row, int column)
            {
                return (column==0) ? false : true;
            }
        };
        Table g = new Table(tableName, columns, r);

        table = new JTable(tm);
        for(String column : columns)
        {
            if(tableb.isFKey(column))
            {
                String tmp = tableb.getFKTable(column);
                Vector<String> someData = new Vector<String>();
                Table fk = null;
                try
                {
                    fk = base.getTable(tmp);
                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
                Vector<String> d = fk.getColumn("ID");
                for(String ko : d)
                {
                    someData.add(fk.mapIDToDescription(ko));
                }

                int vColIndex = tableb.getColumnNum(column);
                TableColumn col = table.getColumnModel().getColumn(vColIndex);
                col.setCellEditor(new MyComboBoxEditor(someData));
                col.setCellRenderer(new MyComboBoxRenderer(someData));
            }
        }
        tm.addTableModelListener(new TableModelListener()
        {
            @Override
            public void tableChanged(TableModelEvent e)
            {
                int row = e.getFirstRow();
                int column = e.getColumn();
                TableModel model = (TableModel) e.getSource();
                String columnName = model.getColumnName(column);
                String kokk = (String) model.getValueAt(row, column);
                System.out.println("KOKOKOKO" + kokk);
                if(!tableb.isFKey(columnName))
                {
                    String data = (String) model.getValueAt(row, column);
                    try
                    {
                        base.updateEntry(tableName, columnName, (String) data, table.getColumnName(0), (String) table.getValueAt(row, 0));
                    }
                    catch(SQLException e1)
                    {
                        e1.printStackTrace();
                        //JOptionPane.showMessageDialog(frame, "SQLException: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    repaintTablePanel();
                }

                else
                {

                    String tname = tableb.getFKTable(columnName);
                    Table t = null;
                    try
                    {
                        t = base.getTable(tname);
                    }
                    catch(SQLException e1)
                    {
                        e1.printStackTrace();
                    }
                    Vector<String> r= t.getColumn("ID");
                    String vvv ="";
                    for(String id: r)
                    {
                        vvv = id;
                        if (t.mapIDToDescription(id).equals(kokk))
                        {
                            break;
                        }
                    }
                    String ko = vvv;
                    try
                    {
                        base.updateEntry(tableName, columnName, ko, table.getColumnName(0), (String) table.getValueAt(row, 0));
                    }
                    catch(SQLException e1)
                    {
                        e1.printStackTrace();
                        //JOptionPane.showMessageDialog(frame, "SQLException: " + e1.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
                    }
                    repaintTablePanel();
                }
            }
        });


        JScrollPane listScrollPane = new JScrollPane(tableList);
        JScrollPane tableScrollPane = new JScrollPane(table);

        listScrollPane.setPreferredSize(new Dimension(250, 80));

        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, listScrollPane, tableScrollPane);
        splitPane.setOneTouchExpandable(true);
        splitPane.setDividerLocation(150);

        listScrollPane.setMinimumSize(new Dimension(100, 50));
        tableScrollPane.setMinimumSize(new Dimension(100, 50));

        tablePanel.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        tablePanel.add(splitPane, c);
        removeAll();
        add(buttonPanel);
        add(tablePanel);
    }
}
