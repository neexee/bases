package fit_9202_machulskis;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

public class Table
{
    Vector<Vector<String>> t = new Vector<Vector<String>>();
    Vector<String> columnNames;
    String name = "";
    HashMap<String, String> fieldToTable = new HashMap<String, String>();
    boolean dirty = true;

    public Table(ResultSet rs)
    {
        ResultSetMetaData rsmd = null;
        try
        {
            rsmd = rs.getMetaData();

            int count = rsmd.getColumnCount();
            Vector<String> columns = new Vector<String>(count);
            for(int i = 1; i <= count; i++)
            {
                String columnName = rsmd.getColumnName(i);
                columns.add(columnName);
            }
            columnNames = columns;

            while(rs.next())
            {
                Vector<String> row = new Vector<String>(count);
                for(int i = 1; i <= count; i++)
                {
                    String columnName = rsmd.getColumnName(i);
                    row.add(rs.getString(columnName));

                }
                addRow(row);
            }

        }
        catch(SQLException e)
        {
            e.printStackTrace();
        }

    }

    public Table(String name, Vector<String> columnNames)
    {
        this.columnNames = columnNames;
        this.name = name;
    }
    public Table(String name, Vector<String> columns, Vector<Vector<String>> data)
    {
        this.name = name;
        this.t = data;
        this.columnNames= columns;
    }
    public void addRow(Vector<String> row)
    {
        t.add(row);
    }

   public String getName()
    {
        return name;
    }
    public int getColumnNum(String colName)
    {
        return columnNames.indexOf(colName);
    }
    public int columnNum()
    {
        if(rowNum()!=0)
            return t.get(0).size();
        else
            return 0;
    }

    public int rowNum()
    {
        return t.size();
    }

    public Vector<String> getRow(int index)
    {
        return t.get(index);
    }

    public Vector<String> getColumn(int index)
    {
        return new Vector<String>();
    }

    public Vector<String> getColumn(String columnName)
    {
      //  System.out.println("WANNA " + columnName);
        Vector<String> tmp = new Vector<String>();
        int index = columnNames.indexOf(columnName);
        for(Vector<String> row : t)
        {
            tmp.add(row.get(index));
        }
        return tmp;
    }

    public void setShowColumns(String[] arr)
    {
    }

    public void setNotShowColumns(String[] arr)
    {

    }

    public Vector<Vector<String>> getData()
    {
        return t;
    }

    @Override
    public String toString()
    {
        String tmp = "";
        for(String columnName : columnNames)
        {
            tmp += columnName + " ";
        }
        tmp += "\n";
        for(Vector<String> row : t)
        {
            for(String l : row)
            {
                tmp += l + " ";
            }
            tmp += "\n";
        }
        return tmp;
    }

    public Vector<String> getDescriptionColumns()
    {
        Vector<String> tmp = new Vector<String>();
     //   System.out.println("NAME DESCRIPTION FOR:"+ name+"'");
        if(name.equals("AREA"))
        {
            tmp.add("DESCRIPTION");
            return tmp;
        }
        if(name.equals("PEOPLES"))
        {
            tmp.add("FIRSTNAME");
            tmp.add("LASTNAME");
            return tmp;
        }
        if(name.equals("TECHNIQUE_TYPE"))
        {
            tmp.add("DESCRIPTION");
            return tmp;
        }
        if(name.equals("INGENEER_TYPES"))
        {
            tmp.add("DESCRIPTION");
            return tmp;
        }
        if(name.equals("WORKER_TYPES"))
        {
            tmp.add("DESCRIPTION");
            return tmp;
        }
        return tmp;
    }
    public boolean isFKey(String column)
    {
        if(fieldToTable.get(column) != null)
        {
            return true;
        }
        return false;
    }
    public void addFKKey(String column, String table)
    {
        fieldToTable.put(column, table);
    }

    public String getFKTable(String column)
    {
        return fieldToTable.get(column);
    }

    public String mapIDToDescription(String id)
    {
        int index = columnNames.indexOf("ID");
        String description = "";
        {
            for(Vector<String> row : t)
            {
                if(row.get(index).equals(id))
                {
                    Vector<String> tmp = getDescriptionColumns();
                    for(String s : tmp)
                    {
                        int r = columnNames.indexOf(s);
                        description += row.get(r) + " ";
                    }
                }
            }
        }
        return description;
    }
    public void setDirtyFlag(boolean f)
    {
       dirty = f;
    }
    public boolean isDirty()
    {
        return dirty;
    }
}
