package fit_9202_machulskis;

import fit_9202_machulskis.query.*;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.*;
import java.sql.SQLException;
import java.util.Vector;

/**
 * User: violetta
 * Date: 5/27/12
 * Time: 5:03 AM
 */
public class QueryPanel extends JPanel
{
        JList queryList;
        Database base;
        JPanel tablePanel;
        int selectedQuery = 0;
        Vector<Query> queries = new Vector<Query>();
        Vector<String> queryNames = new Vector<String>();
        QueryPanel()
        {
            super();
            init();
            initTableList();
     //       repaintTablePanel();
        }

        void setBase(Database base)
        {
            this.base = base;
        }
        void init()
        {
          tablePanel = new TablesPanel();
           
           Query q = new First(this);
           queries.add(q);
           queryNames.add(q.name());
           
            q = new Second(this);
           queries.add(q);
           queryNames.add(q.name());
           q = new Third(this);
           queries.add(q);
           queryNames.add(q.name());
           q = new Fifth(this);
           queries.add(q);
           queryNames.add(q.name());
        }
        void connect()
        {

            initTableList();
            for(Query i: queries)
            {
                i.init();
            }
            repaintTablePanel();

        }
        public void refresh()
        {
           repaintTablePanel();
        }
        void initTableList()
        {
            queryList = new JList();
            queryList.setListData(queryNames);
            queryList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            queryList.setLayoutOrientation(JList.VERTICAL);
            queryList.setSelectedIndex(selectedQuery);
            queryList.addListSelectionListener(new ListSelectionListener()
            {
                //@SuppressWarnings("unchecked")
                @Override
                public void valueChanged(ListSelectionEvent e)
                {
                    queryList = (JList<String>) e.getSource();
                    selectedQuery = queryList.getSelectedIndex();
                    repaintTablePanel();
                    repaint();
                }
            });
        }

        void repaintTablePanel()
        {
            tablePanel.removeAll();
            removeAll();
            tablePanel = new JPanel();

            Vector<String> columns = new Vector<String>();
            final Query query = queries.get(selectedQuery);
            Table tableb = null;
            try
            {
                tableb = base.sendSQL(query.generateQuery());
            }
            catch(SQLException e)
            {
                e.printStackTrace();
            }
            Vector<Vector<String>>v = tableb.getData();
            columns = tableb.columnNames;
            TableModel tm = new DefaultTableModel(v, columns)
            {
                private static final long serialVersionUID = 1L;

                public boolean isCellEditable(int row, int column)
                {
                    return true;
                }
            };
            final JTable table = new JTable(tm);
            JPanel areasPanel = new JPanel();
            areasPanel.setLayout(new GridBagLayout());
            GridBagConstraints cc = new GridBagConstraints();
            
            for(JCheckBox i: query.getCheckBoxes())
            {
                areasPanel.add(i, cc);
            }
            for(JComboBox i : query.getComboBoxes())
            {
                areasPanel.add(i, cc) ;
            }
            JScrollPane listScrollPane = new JScrollPane(queryList);
            JScrollPane tableScrollPane = new JScrollPane(table);

            listScrollPane.setPreferredSize(new Dimension(250, 80));

            JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, listScrollPane, tableScrollPane);
            splitPane.setOneTouchExpandable(true);
            splitPane.setDividerLocation(150);

            listScrollPane.setMinimumSize(new Dimension(100, 50));
            tableScrollPane.setMinimumSize(new Dimension(100, 50));

            tablePanel.setLayout(new GridBagLayout());
            GridBagConstraints c = new GridBagConstraints();
            //	c.fill = GridBagConstraints.VERTICAL;
            c.gridx =0;
            c.gridy =0;
            c.weightx = 1;
            c.weighty = 1;

            tablePanel.add(areasPanel,c );
            c.gridy = 1;
            tablePanel.add(splitPane, c);
            //removeAll();
            add(tablePanel);
        }
    public Database getBase()
    {
        return base;
    }
    }

