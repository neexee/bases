package fit_9202_machulskis;

import javax.swing.*;
import java.awt.*;

/**
 * User: violetta
 * Date: 5/13/12
 * Time: 5:39 PM
 */
public class MainWindow extends JFrame
{
    final String MENU_NAME = "File";
    final String CONNECT_STRING = "Connect";
    final String DISCONNECT_STRING = "Disconnect";
    final String EXIT = "Exit";
    JMenuItem exit;
    JMenuItem connect;
    JMenuItem disconnect;
    JMenuBar menuBar;
    SQLPanel sqlPanel;
    TablesPanel tables;
    QueryPanel queryPanel;
    public MainWindow(String name)
    {
        super(name);

    }
    void drawGUI()
    {
        //mainPanel = new JPanel();
        //mainPanel.setLayout(new GridBagLayout());

        GridBagConstraints c = new GridBagConstraints();

        menuBar = new JMenuBar();

        //JMenu menu = new JMenu(MENU_NAME);

        JMenu menu = new JMenu(MENU_NAME);
        exit = new JMenuItem(EXIT);
        connect = new JMenuItem(CONNECT_STRING);
        disconnect = new JMenuItem(DISCONNECT_STRING);
        //openFile = new JMenuItem(OPEN_FILE);

        exit = new JMenuItem(EXIT);

        menu.add(connect);
        menu.add(disconnect);
        menu.add(exit);
        menuBar.add(menu);
        setJMenuBar(menuBar);
        //getContentPane().add(mainPanel);
        JTabbedPane tabbedPane = new JTabbedPane();
        sqlPanel = new SQLPanel();
        tabbedPane.addTab("SQL", sqlPanel);
        tables = new TablesPanel();
        tabbedPane.addTab("Tables", tables);
        queryPanel = new QueryPanel();
        tabbedPane.addTab("Queries", queryPanel);
        add(tabbedPane);
        pack();
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
    JMenuItem getExitMenuItem()
    {
        return  exit;
    }
    JMenuItem getConnect()
    {
        return connect;
    }
    JMenuItem getDisconnectMenuItem()
    {
        return disconnect;
    }

}
