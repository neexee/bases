package fit_9202_machulskis;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;

/**
 * User: violetta
 * Date: 5/13/12
 * Time: 5:43 PM
 */
public class MainWindowController
{
    MainWindow window;
    Database database;
    MainWindowController()
    {
        this.window = new MainWindow("fit_9202_machulskis.Database");
        database = new Database();

    }
    public void start()
    {
        prepareWindow();
    }
    private void prepareWindow()
    {
        window.drawGUI();
        window.sqlPanel.setBase(database);
        window.tables.setBase(database);
        window.queryPanel.setBase(database);
        JMenuItem exit = window.getExitMenuItem();
        exit.addActionListener(new ActionListener()
        {
            @Override
            public
            void actionPerformed(ActionEvent e)
            {
                window.dispose();
            }
        });
        JMenuItem connect = window.getConnect();
        connect.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                try
                {
                    database.connect("jdbc:oracle:thin:@10.4.0.119:1521:XE", "smachulskis", "12345");
                    window.tables.connect();
                    window.queryPanel.connect();
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }
        });
        JMenuItem disconnect = window.getDisconnectMenuItem();
        disconnect.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent actionEvent)
            {
                try
                {
                    database.disconnect();
                }
                catch(SQLException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }
}
