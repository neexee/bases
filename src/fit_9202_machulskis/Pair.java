package fit_9202_machulskis;

public class Pair<T, S>
{
  public Pair(T f, S s)
  {
    first = f;
    second = s;
  }

  public T first()
  {
    return first;
  }

  public S second()
  {
    return second;
  }

  public String toString()
  {
    return "(" + first.toString() + ", " + second.toString() + ")";
  }

  protected T first;
  protected S second;
}
